package com.example.olga.webapplication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @Expose
    private String name;
    @Expose
    private String coins;
    @Expose
    private String level;
    @SerializedName("homeworks_to_next_level")
    @Expose
    private String homeworksToNextLevel;
    @SerializedName("class_id")
    @Expose
    private String classId;
    @Expose
    private String id;
    @Expose
    private String email;
    @Expose
    private String password;
    @Expose
    private Integer type;

    /**
     *
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     *     The coins
     */
    public String getCoins() {
        return coins;
    }

    /**
     *
     * @param coins
     *     The coins
     */
    public void setCoins(String coins) {
        this.coins = coins;
    }

    /**
     *
     * @return
     *     The level
     */
    public String getLevel() {
        return level;
    }

    /**
     *
     * @param level
     *     The level
     */
    public void setLevel(String level) {
        this.level = level;
    }

    /**
     *
     * @return
     *     The homeworksToNextLevel
     */
    public String getHomeworksToNextLevel() {
        return homeworksToNextLevel;
    }

    /**
     *
     * @param homeworksToNextLevel
     *     The homeworks_to_next_level
     */
    public void setHomeworksToNextLevel(String homeworksToNextLevel) {
        this.homeworksToNextLevel = homeworksToNextLevel;
    }

    /**
     *
     * @return
     *     The classId
     */
    public String getClassId() {
        return classId;
    }

    /**
     *
     * @param classId
     *     The class_id
     */
    public void setClassId(String classId) {
        this.classId = classId;
    }

    /**
     *
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     *     The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     *     The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     *     The password
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     *     The password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return
     *     The type
     */
    public Integer getType() {
        return type;
    }

    /**
     *
     * @param type
     *     The type
     */
    public void setType(Integer type) {
        this.type = type;
    }

}
