package com.example.olga.webapplication;


import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link android.app.Fragment} subclass.
 * Use the {@link HomeworksFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class HomeworksFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    List<String> myList = new ArrayList<>();
    RecyclerView recyclerView;
    List<Subject> l;

    public static HomeworksFragment newInstance(int param1) {
        HomeworksFragment fragment = new HomeworksFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }
    public HomeworksFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_homeworks, container, false);
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        String url ="http://www.followtasks.com/phone/getSubjects";


        Response.Listener<JSONObject> listener = new Response.Listener<JSONObject>(){

            @Override
            public void onResponse(JSONObject response) {
                Gson gson = new Gson();
                ArrayList<Subject> list = gson.fromJson(String.valueOf(response),
                        new TypeToken<ArrayList<Subject>>() {}.getType());
                for(int i = 0; i < list.size(); i++){
                    l.add(list.get(i));
                }
                //Subject subject = gson.fromJson(String.valueOf(response),Subject.class);
                //myList.add(subject.getName());
            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //nameView.setText(error.toString());
            }
        };

        //JsonObjectRequest jsonRequest = new JsonObjectRequest(url, null, listener, errorListener);
        //requestQueue.add(jsonRequest);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Gson gson = new Gson();
                        try {
                            JSONArray jr = new JSONArray(response);
                            for(int i=0;i<jr.length();i++){
                                Subject subject = gson.fromJson(String.valueOf(jr.get(i)),Subject.class);
                                //log(subject.getName());
                                //Toast.makeText(getActivity(), subject.getName(), Toast.LENGTH_SHORT).show();
                                myList.add(subject.getName());
                            }
                            recyclerView.setAdapter(new MyRecyclerAdapter(myList, R.layout.recycler_row));

                        } catch (JSONException e) {
                            Toast.makeText(getActivity(), "error", Toast.LENGTH_SHORT).show();
                        }
                        //Subject subject = gson.fromJson(String.valueOf(response),Subject.class);
                        //Toast.makeText(getActivity(), response, Toast.LENGTH_LONG).show();
                        //mTextView.setText("Response is: "+ response.substring(0,500));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),error.toString(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put("user_id",1+"");
                return params;
            }
        };

        requestQueue.add(stringRequest);

        //Toast.makeText(getActivity(), l.get(0).getName(), Toast.LENGTH_SHORT).show();

        //myList = Arrays.asList("Math", "CZ", "AJ", "CZ", "AJ", "CZ", "AJ", "CZ", "AJ", "CZ", "AJ", "CZ", "AJ", "CZ", "AJ");
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        recyclerView = (RecyclerView) getActivity().findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        super.onViewCreated(view, savedInstanceState);
    }

    public void log(String message){
        String TAG = "message";
        Log.v(TAG, message);
    }
}
