
package com.example.olga.webapplication;

//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

//@Generated("org.jsonschema2pojo")
public class Subject {

    @Expose
    private String id;
    @Expose
    private String name;
    @Expose
    private Integer precents;

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The precents
     */
    public Integer getPrecents() {
        return precents;
    }

    /**
     * 
     * @param precents
     *     The precents
     */
    public void setPrecents(Integer precents) {
        this.precents = precents;
    }

}
