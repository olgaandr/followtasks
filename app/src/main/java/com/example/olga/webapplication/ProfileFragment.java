package com.example.olga.webapplication;


import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link android.app.Fragment} subclass.
 *
 */
public class ProfileFragment extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";

    public static ProfileFragment newInstance(int sectionNumber) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }


    public ProfileFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        final TextView nameView = (TextView) rootView.findViewById(R.id.name);
        final TextView completeView = (TextView) rootView.findViewById(R.id.complete);
        final TextView incompleteView = (TextView) rootView.findViewById(R.id.incomplete);
        final TextView coinsView = (TextView) rootView.findViewById(R.id.coins);
        final TextView levelView = (TextView) rootView.findViewById(R.id.level);

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        String url ="http://www.followtasks.com/phone/getUser";



        Response.Listener<JSONObject> listener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(getActivity(), response.toString(), Toast.LENGTH_SHORT).show();
                /*Gson gson = new Gson();
                User user = gson.fromJson(String.valueOf(response),User.class);
                Toast.makeText(getActivity(), user.getName(), Toast.LENGTH_SHORT).show();
                nameView.setText(user.getName());
                coinsView.setText(user.getCoins()+" Coins");
                levelView.setText(user.getLevel()+"");*/
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //nameView.setText(error.toString());
                log(error.toString());
                Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_LONG).show();
            }
        };

        /*JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, null, listener, errorListener){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put("user_id",1+"");
                return params;
            }
        };
        requestQueue.add(jsonRequest);*/

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id","1");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Gson gson = new Gson();
                        try {
                            JSONObject jo = new JSONObject(response);
                            User user = gson.fromJson(String.valueOf(jo.toString()),User.class);
                            nameView.setText(user.getName());
                            coinsView.setText(user.getCoins()+" Coins");
                            levelView.setText(user.getLevel()+"");
                            //Toast.makeText(getActivity(),user.getName(), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            Toast.makeText(getActivity(),"error", Toast.LENGTH_SHORT).show();
                        }

                        //mTextView.setText("Response is: "+ response.substring(0,500));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),error.toString(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                params.put("user_id",1+"");
                return params;
            }
        };

        requestQueue.add(stringRequest);


        //JsonObjectRequest jsonRequest = new JsonObjectRequest(url, null, listener, errorListener);
        //JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, listener, errorListener){

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        ProgressBar progressBar = (ProgressBar) getActivity().findViewById(R.id.progressBar);

        ObjectAnimator animation = ObjectAnimator.ofInt(progressBar, "progress", 50);
        animation.setDuration(2000);
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();
        super.onViewCreated(view, savedInstanceState);
    }

    public void log(String message){
        String TAG = "message";
        Log.v(TAG, message);
    }
}
